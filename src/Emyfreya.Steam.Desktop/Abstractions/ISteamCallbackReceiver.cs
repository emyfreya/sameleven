﻿namespace Emyfreya.Steam.Desktop.Abstractions;

public interface ISteamCallbackReceiver : IAsyncDisposable
{
    ChannelReader<CallbackEvent> Events { get; }

    /// <summary>
    /// Returns the raw <see cref="CallbackMessage"/> for unknown/unhandled messages.
    /// </summary>
    ChannelReader<CallbackMessage> UnknownEvents { get; }
}
