﻿namespace Emyfreya.Steam.Desktop.Abstractions;

public interface ISteamUser
{
    Result<bool> IsLoggedOn();
    Result<long> GetSteamID();
}
