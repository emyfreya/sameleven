﻿namespace Emyfreya.Steam.Desktop.Abstractions;

public interface ISteamUtils
{
    Result<uint> GetAppId();
}
