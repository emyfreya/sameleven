﻿namespace Emyfreya.Steam.Desktop.Abstractions;

public interface ICallbackMapper
{
    CallbackEvent Map(CallbackMessage message);
}
