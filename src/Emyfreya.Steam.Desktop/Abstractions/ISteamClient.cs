﻿namespace Emyfreya.Steam.Desktop.Abstractions;

public interface ISteamClient : IDisposable
{
    Result<ISteamAppList> AppList { get; }
    Result<ISteamApps> Apps { get; }
    Result<ISteamUser> User { get; }
    Result<ISteamUserStats> UserStats { get; }
    Result<ISteamUtils> Utils { get; }
    Result<int> Pipe { get; }
}
