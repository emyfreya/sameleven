﻿namespace Emyfreya.Steam.Desktop.Abstractions;

public interface ISteamCallbackReader : IDisposable
{
    /// <summary>
    /// Returns the task that is used to run the loop.
    /// <para></para>
    /// Completes when there is no more data available.
    /// </summary>
    Task LoopTask { get; }

    Task<T> WaitForAsync<T>(CancellationToken cancellationToken = default)
        where T : CallbackEvent;

    IDisposable OnUserStatsReceived(Func<UserStatsReceivedEvent, CancellationToken, ValueTask> action);
}
