﻿namespace Emyfreya.Steam.Desktop.Abstractions;

public interface ISteamClientManager : IAsyncDisposable
{
    Result<ISteamCallbackReader> Callbacks { get; }

    Result<bool> IsLoggedOn();
    Result<bool> IsSubcribedApp(uint appId);
    Result<string> GetAppName(uint appId);
    Result<string> GetAppLogo(uint appId);
    Result<uint[]> GetInstalledApps();
    Result<long> GetSteamId();
    Result<uint> GetAppId();
    Result<string[]> GetAchievements();
    /// <summary>
    /// Makes a request right before calling <see cref="GetAchievements"/>, to update the steam's achievements cache.
    /// </summary>
    ValueTask<Result<string[]>> GetAchievementsAsync(CancellationToken cancellationToken = default);
}
