﻿namespace Emyfreya.Steam.Desktop.Abstractions;

public interface ISteamUserStats
{
    Result<string[]> GetAchievements();

    /// <summary>
    /// Produces <see cref="UserStatsReceivedEvent"/> or <see cref="ISteamCallbackReader.OnUserStatsReceived"/>.
    /// </summary>
    Result<bool> RequestCurrentStats();
}
