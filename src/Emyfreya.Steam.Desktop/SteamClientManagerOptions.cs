﻿namespace Emyfreya.Steam.Desktop;

public sealed record SteamClientManagerOptions
{
    public uint? AppId { get; set; }
    public required SteamClientBuildMethod BuildMethod { get; set; }
}
