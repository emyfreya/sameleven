﻿namespace Emyfreya.Steam.Desktop.Models.Errors;

public sealed class CreateSteamAppsError() : Error();
