﻿namespace Emyfreya.Steam.Desktop.Models.Errors;

public sealed class CreateSteamClientError()
    : Error("Could not find any build method, or everything failed.");
