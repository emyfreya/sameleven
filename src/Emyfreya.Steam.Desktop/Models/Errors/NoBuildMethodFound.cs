﻿namespace Emyfreya.Steam.Desktop.Models.Errors;

public sealed class NoBuildMethodFound()
    : Error("No build method found.");
