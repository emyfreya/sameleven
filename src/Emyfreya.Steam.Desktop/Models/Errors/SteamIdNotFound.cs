﻿namespace Emyfreya.Steam.Desktop.Models.Errors;

public sealed class SteamIdNotFound()
    : Error("SteamID was not found.");
