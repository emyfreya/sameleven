﻿using Emyfreya.Steam.Desktop.SteamCallbacks;

namespace Emyfreya.Steam.Desktop.Models.Errors;

public sealed class GetCallbackError : Error
{
    public GetCallbackError(int pipe, CallbackMessage message, int call)
        : base($"Could not get callback for pipe {pipe}.")
    {
        WithMetadata("message", message);
        WithMetadata("call", call);
    }
}
