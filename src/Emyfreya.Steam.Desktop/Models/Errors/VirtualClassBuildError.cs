﻿namespace Emyfreya.Steam.Desktop.Models.Errors;

public sealed class VirtualClassBuildError : Error
{
    public VirtualClassBuildError(nint handle, Type type)
        : base($"Cound not build wrapper around type of '{type}'.")
    {
        WithMetadata("handle", handle);
    }
}
