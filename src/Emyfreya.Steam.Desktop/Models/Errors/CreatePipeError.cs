﻿namespace Emyfreya.Steam.Desktop.Models.Errors;

public sealed class CreatePipeError()
    : Error("Could not create steam pipe.");
