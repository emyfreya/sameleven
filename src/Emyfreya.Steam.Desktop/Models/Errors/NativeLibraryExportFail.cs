﻿namespace Emyfreya.Steam.Desktop.Models.Errors;

public sealed class NativeLibraryExportFail : Error
{
    public NativeLibraryExportFail(nint handle, string name, Exception ex)
        : base($"Could not export method '{name}' library with {handle}.")
    {
        CausedBy(ex);
    }
}
