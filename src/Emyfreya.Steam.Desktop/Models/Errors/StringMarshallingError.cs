﻿namespace Emyfreya.Steam.Desktop.Models.Errors;

public sealed class StringMarshallingError(nint pointer, string delegateName)
    : Error($"Could not marshal pointer '{pointer}' to string for delegate '{delegateName}'.");
