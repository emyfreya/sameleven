﻿namespace Emyfreya.Steam.Desktop.Models;

public abstract record SteamClientBuildMethod()
{
    public static SteamClientBuildMethod FromRegistry(string? dllName = null)
    {
        return new FromRegistryBuildMethod(dllName);
    }

    public static SteamClientBuildMethod FromPath(string DllAbsolutePath)
    {
        return new FromPathSteamBuild(DllAbsolutePath);
    }
}

internal sealed record FromRegistryBuildMethod(string? DllName) : SteamClientBuildMethod;

internal sealed record FromPathSteamBuild(string DllAbsolutePath) : SteamClientBuildMethod();
