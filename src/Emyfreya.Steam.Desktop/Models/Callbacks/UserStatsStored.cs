﻿namespace Emyfreya.Steam.Desktop.Models.Callbacks;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
internal struct UserStatsStored
{
    public uint GameId;
    public SteamErrorResultCode Result;
}

public sealed record UserStatsStoredEvent(uint AppId, SteamErrorResultCode Code) : CallbackEvent;
