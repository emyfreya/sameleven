﻿namespace Emyfreya.Steam.Desktop.Models.Callbacks;

public sealed record SteamServersConnectedEvent() : CallbackEvent;
