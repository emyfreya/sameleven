﻿namespace Emyfreya.Steam.Desktop.Models.Callbacks;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
internal struct UserStatsReceived
{
    public uint GameId;
    public SteamErrorResultCode Result;
    public uint SteamId;
}

public sealed record UserStatsReceivedEvent(uint AppId, SteamErrorResultCode Code, uint SteamId) : CallbackEvent;
