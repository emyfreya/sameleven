﻿namespace Emyfreya.Steam.Desktop.Models;

internal sealed class VirtualClassWrapper<T>
    where T : struct
{
    public T VirtualClass { get; }
    public nint InterfaceHandle { get; }

    public VirtualClassWrapper(T virtualClass, nint interfaceHandle)
    {
        VirtualClass = virtualClass;
        InterfaceHandle = interfaceHandle;
    }

    public static Result<VirtualClassWrapper<T>> Build(nint interfaceHandle)
    {
        return Result.FailIf(interfaceHandle == nint.Zero, new VirtualClassBuildError(interfaceHandle, typeof(T)))
            .Bind(() => Result.Try(() => Marshal.PtrToStructure<VirtualClass>(interfaceHandle).VirtualTable))
            .Bind(virtualClassHandle => Result.Try(() => Marshal.PtrToStructure<T>(virtualClassHandle)))
            .Bind<VirtualClassWrapper<T>>(structure => new VirtualClassWrapper<T>(structure, interfaceHandle));
    }

    public TDelegate GetDelegate<TDelegate>(Func<T, nint> getDelegatePointer) where TDelegate : Delegate
    {
        return Marshal.GetDelegateForFunctionPointer<TDelegate>(getDelegatePointer(VirtualClass));
    }
}
