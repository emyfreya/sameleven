﻿namespace Emyfreya.Steam.Desktop.Models.Interfaces;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct SteamUserStats012
{
    public const string Name = "STEAMUSERSTATS_INTERFACE_VERSION012";

    public nint RequestCurrentStats;
    public nint GetStatInt;
    public nint GetStatFloat;
    public nint SetStatInt;
    public nint SetStateFloat;
    public nint UpdateAvgRateStat;
    public nint GetAchievement;
    public nint SetAchievement;
    public nint ClearAchievement;
    public nint GetAchievementAndUnlockTime;
    public nint StoreStats;
    public nint GetAchievementIcon;
    public nint GetAchievementDisplayAttribute;
    public nint IndicateAchievementProgress;
    public nint GetNumAchievements;
    public nint GetAchievementName;
}
