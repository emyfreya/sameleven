﻿namespace Emyfreya.Steam.Desktop.Models.Interfaces;

[UnmanagedFunctionPointer(CallingConvention.ThisCall)]
internal delegate uint GetAppId(nint self);
