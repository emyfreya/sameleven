﻿namespace Emyfreya.Steam.Desktop.Models.Interfaces;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
internal struct SteamUtils010
{
    public const string Name = "SteamUtils010";

    public nint GetSecondsSinceAppActive;
    public nint GetSecondsSinceComputerActive;
    public nint GetConnectedUniverse;
    public nint GetServerRealTime;
    public nint GetIPCountry;
    public nint GetImageSize;
    public nint GetImageRGBA;
    public nint GetCSERIPPort;
    public nint GetCurrentBatteryPower;
    public nint GetAppID;
}
