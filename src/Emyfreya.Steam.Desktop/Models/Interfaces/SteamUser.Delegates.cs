﻿namespace Emyfreya.Steam.Desktop.Models.Interfaces;

[UnmanagedFunctionPointer(CallingConvention.ThisCall)]
internal delegate void GetSteamID(nint self, ref long steamId);

[UnmanagedFunctionPointer(CallingConvention.ThisCall)]
[return: MarshalAs(UnmanagedType.I1)]
internal delegate bool BLoggedOn(nint self);
