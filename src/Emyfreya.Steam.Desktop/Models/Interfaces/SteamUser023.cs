﻿namespace Emyfreya.Steam.Desktop.Models.Interfaces;

[StructLayout(LayoutKind.Sequential, Pack = 4)]
internal struct SteamUser023
{
    public const string Name = "SteamUser023";

    public nint GetHSteamUser;
    public nint BLoggedOn;
    public nint GetSteamID;
    public nint InitiateGameConnection_DEPRECATED;
    public nint TerminateGameConnection_DEPRECATED;
    public nint TrackAppUsageEvent;
    public nint GetUserDataFolder;
    public nint StartVoiceRecording;
    public nint StopVoiceRecording;
    public nint GetAvailableVoice;
    public nint GetVoice;
    public nint DecompressVoice;
    public nint GetVoiceOptimalSampleRate;
    public nint GetAuthSessionTicket;
    public nint GetAuthTicketForWebApi;
    public nint BeginAuthSession;
    public nint EndAuthSession;
    public nint CancelAuthTicket;
    public nint UserHasLicenseForApp;
    public nint BIsBehindNAT;
    public nint AdvertiseGame;
    public nint RequestEncryptedAppTicket;
    public nint GetEncryptedAppTicket;
    public nint GetGameBadgeLevel;
    public nint GetPlayerSteamLevel;
    public nint RequestStoreAuthURL;
    public nint BIsPhoneVerified;
    public nint BIsTwoFactorEnabled;
    public nint BIsPhoneRequiringVerification;
    public nint GetMarketEligibility;
    public nint GetDurationControl;
    public nint BSetDurationControlOnlineState;
}
