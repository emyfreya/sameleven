﻿namespace Emyfreya.Steam.Desktop.Models.Interfaces;

[UnmanagedFunctionPointer(CallingConvention.ThisCall)]
internal delegate bool RequestCurrentStats(nint self);

[UnmanagedFunctionPointer(CallingConvention.ThisCall)]
internal delegate int GetNumAchievements(nint self);

[UnmanagedFunctionPointer(CallingConvention.ThisCall, CharSet = CharSet.Ansi)]
internal delegate nint GetAchievementName(nint self, uint iAchievement);
