﻿namespace Emyfreya.Steam.Desktop;

/// <summary>
/// Intended to be used as a Singleton, or for the whole lifetime of the application.
/// </summary>
public sealed class SteamClientManager : ISteamClientManager
{
    public Result<ISteamCallbackReader> Callbacks => _callbackReader.Value;

    private readonly Lazy<Result<nint>> _loadLibrary;
    private readonly Lazy<Result<ISteamClient>> _client;
    private readonly Lazy<Result<ISteamCallbackReader>> _callbackReader;
    private readonly Lazy<Result<ISteamCallbackReceiver>> _callbackReceiver;
    private readonly SteamClientManagerOptions _options;

    public SteamClientManager(SteamClientManagerOptions options)
    {
        _options = options;
        _client = new(CreateSteamClient, isThreadSafe: false);
        _loadLibrary = new(LoadLibrary, isThreadSafe: false);
        _callbackReceiver = new(CreateCallbackReceiver, isThreadSafe: false);
        _callbackReader = new(CreateCallbackReader, isThreadSafe: false);
    }

    public async ValueTask DisposeAsync()
    {
        if (_callbackReader.IsValueCreated && _callbackReader.Value.IsSuccess)
        {
            _callbackReader.Value.Value.Dispose();
        }

        if (_callbackReceiver.IsValueCreated && _callbackReceiver.Value.IsSuccess)
        {
            await _callbackReceiver.Value.Value.DisposeAsync().ConfigureAwait(false);
        }

        if (_client.IsValueCreated && _client.Value.IsSuccess)
        {
            _client.Value.Value.Dispose();
        }
    }

    public Result<bool> IsSubcribedApp(uint appId)
    {
        return _client
            .Value
            .Bind(steamClient => steamClient.Apps)
            .Bind<bool>(steamApps => steamApps.IsSubscribedApp(appId));
    }

    public Result<uint[]> GetInstalledApps()
    {
        return _client
            .Value
            .Bind(steamClient => steamClient.AppList)
            .Bind<uint[]>(steamAppList => steamAppList.GetInstalledApps());
    }

    public Result<string> GetAppName(uint appId)
    {
        return _client
            .Value
            .Bind(steamClient => steamClient.Apps)
            .Bind(steamApps => steamApps.GetAppName(appId));
    }

    public Result<string> GetAppLogo(uint appId)
    {
        return _client
            .Value
            .Bind(steamClient => steamClient.Apps)
            .Bind(steamApps => steamApps.GetAppLogo(appId));
    }

    public Result<bool> IsLoggedOn()
    {
        return _client
            .Value
            .Bind(steamClient => steamClient.User)
            .Bind(steamUser => steamUser.IsLoggedOn());
    }

    public Result<long> GetSteamId()
    {
        return _client
            .Value
            .Bind(steamClient => steamClient.User)
            .Bind(steamUser => steamUser.GetSteamID());
    }

    public Result<uint> GetAppId()
    {
        return _client.Value
            .Bind(steamClient => steamClient.Utils)
            .Bind(utils => utils.GetAppId());
    }

    public async ValueTask<Result<string[]>> GetAchievementsAsync(CancellationToken cancellationToken = default)
    {
        if (Callbacks.IsFailed) return Callbacks.ToResult();

        Task<UserStatsReceivedEvent> userStatsReceivedEvent = Callbacks.Value.WaitForAsync<UserStatsReceivedEvent>(cancellationToken);

        Result<bool> requestCurrentStats = _client
           .Value
           .Bind(steamClient => steamClient.UserStats)
           .Bind(stats => stats.RequestCurrentStats());

        if (requestCurrentStats.IsFailed) return requestCurrentStats.ToResult();

        await userStatsReceivedEvent.ConfigureAwait(false);

        return GetAchievements();
    }

    public Result<string[]> GetAchievements()
    {
        return _client
            .Value
            .Bind(steamClient => steamClient.UserStats)
            .Bind(steamUserStats => steamUserStats.GetAchievements());
    }

    private Result<ISteamCallbackReader> CreateCallbackReader()
    {
        return _callbackReceiver
            .Value
            .Bind<ISteamCallbackReader>(manager => new SteamCallbackReader(manager.Events));
    }

    private Result<ISteamCallbackReceiver> CreateCallbackReceiver()
    {
        Result<int> pipe = _client.Value
            .Bind(steamClient => steamClient.Pipe);

        Result<nint> libraryHandle = _loadLibrary.Value;

        return Result.Merge(pipe, libraryHandle)
            .Bind<ISteamCallbackReceiver>(() => new SteamCallbackReceiver(libraryHandle.Value, pipe.Value, new SteamCallbackManagerOptions()));
    }

    private Result<nint> LoadLibrary()
    {
        if (_options.AppId is { } appId)
        {
            Environment.SetEnvironmentVariable(SteamConsts.SteamAppIdEnvKey, appId.ToString(CultureInfo.InvariantCulture));
        }

        return _options.BuildMethod switch
        {
            FromPathSteamBuild fromPath => SteamClientFactory.BuildFromPath(fromPath.DllAbsolutePath),
            FromRegistryBuildMethod fromRegistry => SteamClientFactory.BuildFromRegistry(fromRegistry.DllName ?? SteamConsts.SteamClientDllName),
            _ => Result.Fail(new NoBuildMethodFound()),
        };
    }

    private Result<ISteamClient> CreateSteamClient()
    {
        return _loadLibrary
            .Value
            .Bind(SteamClientFactory.CreateSteamClient);
    }
}
