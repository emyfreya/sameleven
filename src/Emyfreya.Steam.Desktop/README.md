﻿# README

This library is to communicate with `steam.exe`, through `steamclient.dll`, with .NET 8 Windows (`net8.0-windows`).

It is **NOT** another wrapped client around `steam_api.dll`, although it could work.

Most of the code is guess work.

## Specifics

This library only works with other library compiled in x86.

Only one instance of `SteamClientManager` can work, has the native library is loaded into memory, 
it automatically searches for the `SteamAppId` environment variable.

It is, as of now, impossible to (re)load the native library and change the `SteamAppId` value if it has already been loaded into memory.

## How to use

Instanciate `SteamClientManager`.

ℹ It uses [FluentResults](https://github.com/altmann/FluentResults) to know if an operation has failed.
It could fail for multiple reasons, such as the library did not load or can't map a method to the loaded native library.

Use the `.Bind` method to do something only if the operation has succeeded.

ℹ This library does not throw any exceptions.

```csharp
SteamClientManagerOptions options = new()
{
  AppId = 440, // Not required, some methods might not work as intended if not specified.
  BuildMethod = SteamClientBuildMethod.FromRegistry()
};
await using SteamClientManager manager = new(options);

Result<string[]> achievements = await manager.GetAchievementsAsync();
achievements.Bind(DoSomething);

void DoSomething(string[] achievements)
{
  // Do something
}
```

## Credits to

- [gibbed's SteamAchievementManager](https://github.com/gibbed/SteamAchievementManager)
- [FluentResults](https://github.com/altmann/FluentResults)