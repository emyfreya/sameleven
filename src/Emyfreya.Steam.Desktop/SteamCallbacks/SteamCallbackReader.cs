﻿namespace Emyfreya.Steam.Desktop.SteamCallbacks;

/// <summary>
/// Instantly runs a background reader to catch steam's API callbacks.
/// <para/>
/// Makes it easy to propose easy to subscribe handlers for the user of the public API, without managing any state.
/// </summary>
internal sealed class SteamCallbackReader : ISteamCallbackReader
{
    public Task LoopTask { get; }

    private readonly Dictionary<Type, List<ICallbackHandler>> _handlersCache = new(capacity: 1);
    private readonly CancellationTokenSource _ctsLoop;
    private readonly ChannelReader<CallbackEvent> _channelReader;

    public SteamCallbackReader(ChannelReader<CallbackEvent> channelReader)
    {
        _channelReader = channelReader;
        _ctsLoop = new CancellationTokenSource();

        LoopTask = StartAsync();
    }

    public void Dispose()
    {
        _ctsLoop.Cancel();
        _ctsLoop.Dispose();

        foreach (List<ICallbackHandler> handlerList in _handlersCache.Values)
        {
            foreach (ICallbackHandler handler in handlerList)
            {
                handler.Dispose();
            }

            handlerList.Clear();
        }

        _handlersCache.Clear();
    }

    public IDisposable OnUserStatsReceived(Func<UserStatsReceivedEvent, CancellationToken, ValueTask> action)
    {
        return RegisterHandler(action);
    }

    private AnonymousCallbackHandler RegisterHandler<TEvent>(Func<TEvent, CancellationToken, ValueTask> action)
        where TEvent : CallbackEvent
    {
        Type eventType = typeof(TEvent);

        List<ICallbackHandler> handlers;

        if (!_handlersCache.TryGetValue(eventType, out handlers!))
        {
            handlers = new List<ICallbackHandler>(capacity: 1);
            _handlersCache[eventType] = handlers;
        }

        AnonymousCallbackHandler handler = new(
            (e, ct) => action((TEvent)e, ct),
            (self) =>
            {
                handlers.Remove(self);
            });

        handlers.Add(handler);

        return handler;
    }

    public async Task<TEvent> WaitForAsync<TEvent>(CancellationToken cancellationToken = default)
        where TEvent : CallbackEvent
    {
        TaskCompletionSource<TEvent> tcs = new(TaskCreationOptions.RunContinuationsAsynchronously);
        cancellationToken.Register(() => tcs.TrySetCanceled(cancellationToken), useSynchronizationContext: false);

        AnonymousCallbackHandler handler = RegisterHandler<TEvent>((e, ct) =>
        {
            tcs.TrySetResult(e);

            return ValueTask.CompletedTask;
        });

        try
        {
            return await tcs.Task.ConfigureAwait(false);
        }
        finally
        {
            handler.Dispose();
        }
    }

    private async Task StartAsync()
    {
        CancellationToken cancellationToken = _ctsLoop.Token;

        while (!cancellationToken.IsCancellationRequested)
        {
            if (_channelReader.Completion.IsCompleted)
            {
                _ctsLoop.Cancel();

                break;
            }

            CallbackEvent callbackEvent = await _channelReader.ReadAsync(cancellationToken).ConfigureAwait(false);

            if (!_handlersCache.TryGetValue(callbackEvent.GetType(), out List<ICallbackHandler>? handlers) || handlers.Count <= 0)
            {
                continue;
            }

            foreach (ICallbackHandler handler in handlers)
            {
                await handler.HandleAsync(callbackEvent, cancellationToken).ConfigureAwait(false);
            }
        }
    }
}
