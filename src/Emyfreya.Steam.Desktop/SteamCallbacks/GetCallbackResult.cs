﻿namespace Emyfreya.Steam.Desktop.SteamCallbacks;

internal sealed record GetCallbackResult(
    CallbackMessage Message,
    int Call);
