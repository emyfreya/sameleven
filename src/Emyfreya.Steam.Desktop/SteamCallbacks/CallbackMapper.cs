﻿namespace Emyfreya.Steam.Desktop.SteamCallbacks;

internal abstract class CallbackMapper<TFrom, TTo> : ICallbackMapper
    where TFrom : struct
    where TTo : CallbackEvent
{
    public CallbackEvent Map(CallbackMessage message) => Map(Marshal.PtrToStructure<TFrom>(message.ParamPointer));

    public abstract TTo Map(TFrom message);
}

internal abstract class CallbackMapper<TTo> : ICallbackMapper
    where TTo : CallbackEvent
{
    public CallbackEvent Map(CallbackMessage message) => Map();
    public abstract TTo Map();
}

internal sealed class AnonymousCallbackMapper : CallbackMapper<CallbackEvent>
{
    private readonly Func<CallbackEvent> _mapperFunc;

    public AnonymousCallbackMapper(Func<CallbackEvent> mapperFunc)
    {
        _mapperFunc = mapperFunc;
    }

    public static ICallbackMapper Build(Func<CallbackEvent> mapperFunc)
    {
        return new AnonymousCallbackMapper(mapperFunc);
    }

    public override CallbackEvent Map() => _mapperFunc();
}

internal sealed class AnonymousCallbackMapper<TFrom> : CallbackMapper<TFrom, CallbackEvent>
    where TFrom : struct
{
    private readonly Func<TFrom, CallbackEvent> _mapperFunc;

    public AnonymousCallbackMapper(Func<TFrom, CallbackEvent> mapperFunc)
    {
        _mapperFunc = mapperFunc;
    }

    public static ICallbackMapper Build(Func<TFrom, CallbackEvent> mapperFunc)
    {
        return new AnonymousCallbackMapper<TFrom>(mapperFunc);
    }

    public override CallbackEvent Map(TFrom message) => _mapperFunc(message);
}
