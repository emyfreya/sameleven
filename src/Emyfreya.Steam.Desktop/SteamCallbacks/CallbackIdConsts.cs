﻿namespace Emyfreya.Steam.Desktop.SteamCallbacks;

/// <summary>
/// Ids for all known callbacks, available in <c>steam_api.json</c> of the Steamworks source code.
/// </summary>
internal static class CallbackIdConsts
{
    public const int SteamServersConnected = 101;
    public const int SteamServerConnectFailure = 102;
    public const int UserStatsReceived = 1101;
    public const int UserStatsStored = 1102;
    public const int UserAchievementStored = 1103;
    public const int RemoteStorageFileReadAsyncComplete = 1332;
    public const int RemoteStorageLocalFileChange = 1333;
}
