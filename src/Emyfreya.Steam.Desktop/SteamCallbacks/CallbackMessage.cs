﻿namespace Emyfreya.Steam.Desktop.SteamCallbacks;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct CallbackMessage
{
    public int User;
    public int Id;
    public nint ParamPointer;
    public int ParamSize;
}
