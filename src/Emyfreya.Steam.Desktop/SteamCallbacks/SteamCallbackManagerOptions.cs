﻿namespace Emyfreya.Steam.Desktop.SteamCallbacks;

internal sealed record SteamCallbackManagerOptions
{
    public int UnknownCallbackChannelCapacity { get; set; } = 10;

    /// <summary>
    /// Sets the frequency of the callback loop. 
    /// 
    /// <see href="https://partner.steamgames.com/doc/api/steam_api#SteamAPI_RunCallbacks">Recommended is 10Hz (100ms), or once per frame.</see>
    /// </summary>
    public TimeSpan LoopPeriod { get; init; } = TimeSpan.FromMilliseconds(100);

    public Dictionary<int, ICallbackMapper> Mappers { get; }

    public SteamCallbackManagerOptions()
    {
        Mappers = new Dictionary<int, ICallbackMapper>(capacity: 3)
        {
            {
                CallbackIdConsts.SteamServersConnected,
                AnonymousCallbackMapper.Build(() => new SteamServersConnectedEvent())
            },
            {
                CallbackIdConsts.UserStatsReceived,
                AnonymousCallbackMapper<UserStatsReceived>.Build(message => new UserStatsReceivedEvent(message.GameId, message.Result, message.SteamId))
            },
            {
                CallbackIdConsts.UserStatsStored,
                AnonymousCallbackMapper<UserStatsStored>.Build(message => new UserStatsStoredEvent(message.GameId, message.Result))
            }
        };
    }
}
