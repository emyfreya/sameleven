﻿namespace Emyfreya.Steam.Desktop.SteamCallbacks;

/// <summary>
/// This is in charge of running the loop to fetch the callbacks.
/// <para />
/// To allow non blocking receiving, it uses <see cref="Channel"/>s.
/// </summary>
internal sealed class SteamCallbackReceiver : ISteamCallbackReceiver
{
    public ChannelReader<CallbackEvent> Events => _callbackChannel.Reader;
    public ChannelReader<CallbackMessage> UnknownEvents => _unknownCallbackChannel.Reader;

    private readonly Channel<CallbackEvent> _callbackChannel;
    private readonly Channel<CallbackMessage> _unknownCallbackChannel;
    private readonly Timer _timer;
    private readonly nint _libraryHandle;
    private readonly int _pipe;
    private readonly ReadOnlyDictionary<int, ICallbackMapper> _mappers;

    public SteamCallbackReceiver(
        nint libraryHandle,
        int pipe,
        SteamCallbackManagerOptions options)
    {
        _mappers = options.Mappers.AsReadOnly();

        _unknownCallbackChannel = Channel.CreateBounded<CallbackMessage>(new BoundedChannelOptions(capacity: options.UnknownCallbackChannelCapacity)
        {
            FullMode = BoundedChannelFullMode.DropOldest,
            AllowSynchronousContinuations = false,
            SingleWriter = true,
            SingleReader = false
        });

        _callbackChannel = Channel.CreateUnbounded<CallbackEvent>(new UnboundedChannelOptions()
        {
            AllowSynchronousContinuations = false,
            SingleWriter = true,
            SingleReader = false
        });

        _timer = new Timer(TimerCallback, this, TimeSpan.FromSeconds(5), options.LoopPeriod);
        _libraryHandle = libraryHandle;
        _pipe = pipe;
    }

    public async ValueTask DisposeAsync()
    {
        await _timer.DisposeAsync().ConfigureAwait(false);

        _callbackChannel.Writer.TryComplete();
        _unknownCallbackChannel.Writer.TryComplete();
    }

    private static void TimerCallback(object? state)
    {
        SteamCallbackReceiver manager = (SteamCallbackReceiver)state!;
        manager.FetchSteamCallbacks();
    }

    private Result<bool> FetchSteamCallbacks()
    {
        return GetCallback(_pipe)
            .Bind(result => FetchSteamCallbacks(_pipe, result));
    }

    private Result<bool> FetchSteamCallbacks(int pipe, GetCallbackResult result)
    {
        if (_mappers.TryGetValue(result.Message.Id, out ICallbackMapper? mapper))
        {
            _callbackChannel.Writer.TryWrite(mapper.Map(result.Message));
        }
        else
        {
            _unknownCallbackChannel.Writer.TryWrite(result.Message);
        }

        return FreeLastCallback(pipe);
    }

    private Result<GetCallbackResult> GetCallback(int pipe)
    {
        Result<GetCallback> result = ExportDelegate<GetCallback>("Steam_BGetCallback");
        if (result.IsFailed) return result.ToResult();

        bool isSuccessful = result.Value(pipe, out CallbackMessage message, out int call);

        if (!isSuccessful) return Result.Fail(new GetCallbackError(pipe, message, call));

        return new GetCallbackResult(message, call);
    }

    private Result<bool> FreeLastCallback(int pipe)
    {
        Result<FreeLastCallback> result = ExportDelegate<FreeLastCallback>("Steam_FreeLastCallback");
        if (result.IsFailed) return result.ToResult();

        return result.Value(pipe);
    }

    private Result<T> ExportDelegate<T>(string name)
    {
        return Result.Try(() => NativeLibrary.GetExport(_libraryHandle, name), e => new NativeLibraryExportFail(_libraryHandle, name, e))
            .Bind<T>(exportHandle => Marshal.GetDelegateForFunctionPointer<T>(exportHandle));
    }
}
