﻿namespace Emyfreya.Steam.Desktop.SteamCallbacks;

internal sealed class AnonymousCallbackHandler : ICallbackHandler
{
    private readonly Action<ICallbackHandler> _dispose;
    private readonly Func<CallbackEvent, CancellationToken, ValueTask> _handler;

    public AnonymousCallbackHandler(Func<CallbackEvent, CancellationToken, ValueTask> handler, Action<ICallbackHandler> dispose)
    {
        _handler = handler;
        _dispose = dispose;
    }

    public ValueTask HandleAsync(CallbackEvent e, CancellationToken cancellationToken) => _handler(e, cancellationToken);

    public void Dispose()
    {
        _dispose(this);
    }
}

internal interface ICallbackHandler : IDisposable
{
    ValueTask HandleAsync(CallbackEvent e, CancellationToken cancellationToken);
}
