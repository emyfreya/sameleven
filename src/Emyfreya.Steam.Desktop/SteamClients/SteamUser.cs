﻿namespace Emyfreya.Steam.Desktop.SteamClients;

internal sealed class SteamUser : ISteamUser
{
    private readonly VirtualClassWrapper<SteamUser023> _wrapper;

    public SteamUser(VirtualClassWrapper<SteamUser023> wrapper)
    {
        _wrapper = wrapper;
    }

    public Result<bool> IsLoggedOn()
    {
        return _wrapper.GetDelegate<BLoggedOn>(i => i.BLoggedOn)(_wrapper.InterfaceHandle);
    }

    public Result<long> GetSteamID()
    {
        long steamId = 0;
        _wrapper.GetDelegate<GetSteamID>(i => i.GetSteamID)(_wrapper.InterfaceHandle, ref steamId);

        if (steamId == 0) return Result.Fail<long>(new SteamIdNotFound());

        return steamId;
    }
}
