﻿namespace Emyfreya.Steam.Desktop.SteamClients;

/// <summary>
/// The client that wraps <c>steamclient.dll</c>.
/// </summary>
internal sealed class SteamClient : ISteamClient
{
    public Result<ISteamApps> Apps => _apps.Value;
    public Result<ISteamAppList> AppList => _appList.Value;
    public Result<ISteamUser> User => _user.Value;
    public Result<ISteamUserStats> UserStats => _userStats.Value;
    public Result<ISteamUtils> Utils => _utils.Value;
    public Result<int> Pipe => GetOrCreateState().Map(s => s.Pipe);

    private SteamClientState _state;
    private readonly VirtualClassWrapper<SteamClient018> _wrapper;
    private readonly Lazy<Result<ISteamApps>> _apps;
    private readonly Lazy<Result<ISteamAppList>> _appList;
    private readonly Lazy<Result<ISteamUser>> _user;
    private readonly Lazy<Result<ISteamUserStats>> _userStats;
    private readonly Lazy<Result<ISteamUtils>> _utils;

    public SteamClient(VirtualClassWrapper<SteamClient018> wrapper)
    {
        _state = SteamClientState.Empty;
        _wrapper = wrapper;
        _apps = new(CreateSteamApps, isThreadSafe: false);
        _appList = new(CreateSteamAppList, isThreadSafe: false);
        _user = new(CreateSteamUser, isThreadSafe: false);
        _userStats = new(CreateSteamUserStats, isThreadSafe: false);
        _utils = new(CreateSteamUtils, isThreadSafe: false);
    }

    public void Dispose()
    {
        ReleaseState();
        ShutdownIfAllPipesClosed();
    }

    private bool ShutdownIfAllPipesClosed()
    {
        return _wrapper.GetDelegate<BShutdownIfAllPipesClosed>(w => w.ShutdownIfAllPipesClosed)(_wrapper.InterfaceHandle);
    }

    private void ReleaseState()
    {
        if (_state.IsInitialized)
        {
            ReleaseUser(_state.Pipe, _state.User);
            ReleaseSteamPipe(_state.Pipe);

            _state = SteamClientState.Empty;
        }
    }

    private Result<int> CreateSteamPipe()
    {
        int pipe = _wrapper.GetDelegate<CreateSteamPipe>(v => v.CreateSteamPipe)(_wrapper.InterfaceHandle);

        if (pipe == 0) return Result.Fail(new CreatePipeError());

        return pipe;
    }

    private Result<int> ConnectToGlobalUser(int pipe)
    {
        int user = _wrapper.GetDelegate<ConnectToGlobalUser>(v => v.ConnectToGlobalUser)(_wrapper.InterfaceHandle, pipe);

        if (user == 0) return Result.Fail(new ConnectToGlobalUserError(pipe));

        return user;
    }

    private void ReleaseSteamPipe(int pipe)
    {
        _wrapper.GetDelegate<ReleaseSteamPipe>(v => v.ReleaseSteamPipe)(_wrapper.InterfaceHandle, pipe);
    }

    private void ReleaseUser(int pipe, int user)
    {
        _wrapper.GetDelegate<ReleaseUser>(v => v.ReleaseUser)(_wrapper.InterfaceHandle, pipe, user);
    }

    private Result<SteamClientState> GetOrCreateState()
    {
        if (!_state.IsPipeDefined)
        {
            Result<int> pipe = CreateSteamPipe();
            if (pipe.IsFailed) return pipe.ToResult();

            _state = _state with { Pipe = pipe.Value };
        }

        if (!_state.IsUserDefined)
        {
            Result<int> user = ConnectToGlobalUser(_state.Pipe);
            if (user.IsFailed) return user.ToResult();

            _state = _state with { User = user.Value };
        }

        return _state;
    }

    private Result<ISteamApps> CreateSteamApps()
    {
        return GetOrCreateState()
            .Bind(state => CreateSteamApps(state.User, state.Pipe));
    }

    private Result<ISteamAppList> CreateSteamAppList()
    {
        return GetOrCreateState()
            .Bind(state => CreateSteamAppList(state.User, state.Pipe));
    }

    private Result<ISteamUser> CreateSteamUser()
    {
        return GetOrCreateState()
            .Bind(state => CreateSteamUser(state.User, state.Pipe));
    }

    private Result<ISteamUserStats> CreateSteamUserStats()
    {
        return GetOrCreateState()
            .Bind(state => CreateSteamUserStats(state.User, state.Pipe));
    }

    private Result<ISteamUtils> CreateSteamUtils()
    {
        return GetOrCreateState()
            .Bind(state => CreateSteamUtils(state.Pipe));
    }

    private Result<ISteamUtils> CreateSteamUtils(int pipe)
    {
        nint handle = _wrapper.GetDelegate<GetISteamUtils>(v => v.GetISteamUtils)(_wrapper.InterfaceHandle, pipe, SteamUtils010.Name);

        return VirtualClassWrapper<SteamUtils010>.Build(handle)
            .Bind<ISteamUtils>(w => new SteamUtils(w));
    }

    private Result<ISteamUserStats> CreateSteamUserStats(int user, int pipe)
    {
        nint handle = _wrapper.GetDelegate<GetISteamUserStats>(v => v.GetISteamUserStats)(_wrapper.InterfaceHandle, user, pipe, SteamUserStats012.Name);

        return VirtualClassWrapper<SteamUserStats012>.Build(handle)
            .Bind<ISteamUserStats>(w => new SteamUserStats(w));
    }

    private Result<ISteamUser> CreateSteamUser(int user, int pipe)
    {
        nint handle = _wrapper.GetDelegate<GetISteamUser>(v => v.GetISteamUser)(_wrapper.InterfaceHandle, user, pipe, SteamUser023.Name);

        return VirtualClassWrapper<SteamUser023>.Build(handle)
            .Bind<ISteamUser>(w => new SteamUser(w));
    }

    private Result<ISteamApps> CreateSteamApps(int user, int pipe)
    {
        nint steamApps001Handle = _wrapper.GetDelegate<GetISteamApps>(v => v.GetISteamApps)(_wrapper.InterfaceHandle, user, pipe, SteamApps001.Name);
        Result<VirtualClassWrapper<SteamApps001>> steamApps001 = VirtualClassWrapper<SteamApps001>.Build(steamApps001Handle);

        nint steamApps008Handle = _wrapper.GetDelegate<GetISteamApps>(v => v.GetISteamApps)(_wrapper.InterfaceHandle, user, pipe, SteamApps008.Name);
        Result<VirtualClassWrapper<SteamApps008>> steamApps008 = VirtualClassWrapper<SteamApps008>.Build(steamApps008Handle);

        return Result.Merge(steamApps001, steamApps008)
            .Bind<ISteamApps>(() => new SteamApps(steamApps001.Value, steamApps008.Value));
    }

    private Result<ISteamAppList> CreateSteamAppList(int user, int pipe)
    {
        nint handle = _wrapper.GetDelegate<GetISteamAppList>(v => v.GetISteamAppList)(_wrapper.InterfaceHandle, user, pipe, SteamAppList001.Name);

        return VirtualClassWrapper<SteamAppList001>.Build(handle)
            .Bind<ISteamAppList>(w => new SteamAppList(w));
    }
}

