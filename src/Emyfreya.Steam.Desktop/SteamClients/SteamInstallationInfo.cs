﻿namespace Emyfreya.Steam.Desktop.SteamClients;

public sealed record SteamInstallationInfo(
    string InstallPath,
    string Language,
    IEnumerable<string> AppsIds);
