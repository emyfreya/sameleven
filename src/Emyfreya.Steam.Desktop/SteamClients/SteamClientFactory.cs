﻿namespace Emyfreya.Steam.Desktop.SteamClients;

internal static class SteamClientFactory
{
    public static Result<nint> BuildFromRegistry(string dllName)
    {
        Result<SteamInstallationInfo> installationInfo = SteamInstallationInfoFactory.FromRegistry();

        if (installationInfo.IsFailed) return installationInfo.ToResult<nint>();

        string path = Path.Combine(installationInfo.Value.InstallPath, dllName);

        return BuildFromPath(path);
    }

    public static Result<nint> BuildFromPath(string dllPath)
    {
        return Result
            .Try(() => NativeLibrary.Load(dllPath), ex => new NativeLibraryLoadFailed(dllPath, ex));
    }

    public static Result<ISteamClient> CreateSteamClient(nint handle)
    {
        Result<VirtualClassWrapper<SteamClient018>> steamClient018 = CreateInterface<SteamClient018>(handle, SteamClient018.Name);
        if (steamClient018.IsFailed) return steamClient018.ToResult<ISteamClient>();

        return new SteamClient(steamClient018.Value);
    }

    public static nint CreateInterface(nint handle, string version, out uint code)
    {
        nint createInterfacePointer = NativeLibrary.GetExport(handle, "CreateInterface");
        CreateInterface createInterfaceDelegate = Marshal.GetDelegateForFunctionPointer<CreateInterface>(createInterfacePointer);

        code = 0;
        return createInterfaceDelegate(version, ref code);
    }

    public static Result<VirtualClassWrapper<T>> CreateInterface<T>(nint handle, string version)
        where T : struct
    {
        nint interfaceHandle = CreateInterface(handle, version, out uint code);

        if (code != 0) return Result.Fail(new SteamCreateInterfaceCodeError(handle, version, code));

        return VirtualClassWrapper<T>.Build(interfaceHandle);
    }
}
