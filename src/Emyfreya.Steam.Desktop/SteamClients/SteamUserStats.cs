﻿namespace Emyfreya.Steam.Desktop.SteamClients;

internal sealed class SteamUserStats : ISteamUserStats
{
    private readonly VirtualClassWrapper<SteamUserStats012> _wrapper;

    public SteamUserStats(VirtualClassWrapper<SteamUserStats012> wrapper)
    {
        _wrapper = wrapper;
    }

    public Result<string[]> GetAchievements()
    {
        int num = _wrapper.GetDelegate<GetNumAchievements>(w => w.GetNumAchievements)(_wrapper.InterfaceHandle);

        string[] achievements = new string[num];

        for (uint i = 0; i < num; i++)
        {
            Result<string> nameResult = GetAchievementName(i);

            achievements[i] = nameResult.IsFailed
                ? nameResult.Errors.FirstOrDefault()?.Message ?? string.Empty
                : nameResult.Value;
        }

        return achievements;
    }

    public Result<string> GetAchievementName(uint index)
    {
        return Result.Try(() => _wrapper.GetDelegate<GetAchievementName>(w => w.GetAchievementName)(_wrapper.InterfaceHandle, index))
            .Bind<string>(pointer => Marshal.PtrToStringAnsi(pointer) is { } name
                ? name
                : Result.Fail(new StringMarshallingError(pointer, nameof(GetAchievementName))));
    }

    /// <summary>
    /// UserStatsReceived_t
    /// </summary>
    public Result<bool> RequestCurrentStats()
    {
        return _wrapper.GetDelegate<RequestCurrentStats>(w => w.RequestCurrentStats)(_wrapper.InterfaceHandle);
    }
}
