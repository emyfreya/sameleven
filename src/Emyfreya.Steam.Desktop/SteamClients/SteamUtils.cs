﻿namespace Emyfreya.Steam.Desktop.SteamClients;

internal sealed class SteamUtils : ISteamUtils
{
    private readonly VirtualClassWrapper<SteamUtils010> _wrapper;

    public SteamUtils(VirtualClassWrapper<SteamUtils010> wrapper)
    {
        _wrapper = wrapper;
    }

    public Result<uint> GetAppId()
    {
        return _wrapper.GetDelegate<GetAppId>(w => w.GetAppID)(_wrapper.InterfaceHandle);
    }
}
