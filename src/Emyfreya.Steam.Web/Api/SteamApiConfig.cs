﻿namespace Emyfreya.Steam.Web.Api;

public sealed record SteamApiConfig()
{
    public const string Key = "SteamApi";
};
