﻿namespace Emyfreya.Steam.Web.Api;

[JsonSerializable(typeof(GetAppListResult))]
public partial class SteamApiJsonContext : JsonSerializerContext;
