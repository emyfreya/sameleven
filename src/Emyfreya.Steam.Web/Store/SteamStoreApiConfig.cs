﻿namespace Emyfreya.Steam.Web.Store;

public sealed record SteamStoreApiConfig()
{
    public const string Key = "SteamStoreApi";
};
