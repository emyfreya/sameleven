﻿namespace Emyfreya.Steam.Web.Community;

internal sealed record SteamCommunityApiConfig()
{
    public const string Key = "SteamCommunityApi";
}
