﻿using ProblemDetails = Microsoft.AspNetCore.Mvc.ProblemDetails;

namespace Emyfreya.Steam.AppEmu;

internal static class ProblemDetailsFactory
{
    public static ProblemDetails Create<T>(Result<T> result)
    {
        return new FluentResultProblemDetails
        {
            Title = result.Errors[0].Message,
            Detail = "The result has returned with a failed status",
            Status = 400,
            Type = "https://tools.ietf.org/html/rfc7231#section-6.5.1",
            Errors = result.Errors.Select(e => new FluentResultError(e.GetType().Name, e.Message)),
        };
    }
}

internal sealed record FluentResultError(string name, string message);

internal sealed class FluentResultProblemDetails : ProblemDetails
{
    public IEnumerable<FluentResultError>? Errors { get; set; }
}
