﻿namespace Emyfreya.Steam.AppEmu;

[JsonSerializable(typeof(FluentResultProblemDetails))]
[JsonSerializable(typeof(IsSubcribedAppResponse))]
[JsonSerializable(typeof(GetAchievementResponse))]
internal partial class AppJsonSerializerContext : JsonSerializerContext;
