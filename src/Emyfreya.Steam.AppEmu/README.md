# README

## What's this ?

When launched, this emulates a Steam game.

It is possible to pass arguments to emulate one specific game, to then change Steam data about it.

This API is just there to act as a pass through.

## Why ?

The problem is that the SteamLibrary, once loaded in memory, cannot be unloaded to change the SteamAppId.

This project serves as a server that can be spawned in a isolated process.

## How ?

This uses the NamedPipe system on Windows, to avoid communicating by TCP & allocating ports.

Specifically, this creates a named pipe server.

## Usage

`app_emu.exe --app-id 440` launches the server while emulating Team Fortress 2.

## Documentation

- [Using named pipes with ASP.NET Core and HttpClient](https://andrewlock.net/using-named-pipes-with-aspnetcore-and-httpclient/)