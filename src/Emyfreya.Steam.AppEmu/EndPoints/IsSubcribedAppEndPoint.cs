﻿namespace Emyfreya.Steam.AppEmu.EndPoints;

internal sealed record IsSubcribedAppRequest
{
    [Required]
    [BindFrom("id")]
    public required uint AppId { get; init; }
}
internal sealed record IsSubcribedAppResponse(uint AppId, bool IsSubcribed);

internal sealed class IsSubcribedAppEndPoint : Endpoint<IsSubcribedAppRequest>
{
    private readonly ISteamClientManager _steamClientManager;

    public IsSubcribedAppEndPoint(ISteamClientManager steamClientManager)
    {
        _steamClientManager = steamClientManager;
    }

    public override void Configure()
    {
        Get("/apps/{id}/issubcribed");
    }

    public override Task HandleAsync(IsSubcribedAppRequest req, CancellationToken ct)
    {
        Result<bool> result = _steamClientManager.IsSubcribedApp(req.AppId);

        if (result.IsFailed)
        {
            return SendResultAsync(TypedResults.Problem(ProblemDetailsFactory.Create(result)));
        }

        return SendOkAsync(new IsSubcribedAppResponse(req.AppId, result.Value), ct);
    }
}
