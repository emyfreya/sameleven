﻿namespace Emyfreya.Steam.AppEmu.EndPoints;

internal sealed class ExitEndPoint : EndpointWithoutRequest
{
    public override void Configure()
    {
        Get("/exit");
    }

    public override Task HandleAsync(CancellationToken cancellationToken)
    {
        Environment.Exit(0);

        return Task.CompletedTask;
    }
}
