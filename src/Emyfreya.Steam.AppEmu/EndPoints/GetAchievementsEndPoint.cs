﻿namespace Emyfreya.Steam.AppEmu.EndPoints;

internal sealed record GetAchievementResponse(int Count, string[] Achievements);

internal sealed class GetAchievementsEndPoint : EndpointWithoutRequest<GetAchievementResponse>
{
    private readonly ISteamClientManager _steamClientManager;

    public GetAchievementsEndPoint(ISteamClientManager steamClientManager)
    {
        _steamClientManager = steamClientManager;
    }

    public override void Configure()
    {
        Get("/user/stats/achievements");
    }

    public override async Task HandleAsync(CancellationToken cancellationToken)
    {
        Result<string[]> result = await _steamClientManager.GetAchievementsAsync(cancellationToken);

        if (result.IsFailed)
        {
            await SendResultAsync(TypedResults.Problem(ProblemDetailsFactory.Create(result)));
        }
        else
        {
            await SendOkAsync(new GetAchievementResponse(result.Value.Length, result.Value), cancellationToken);
        }
    }
}
