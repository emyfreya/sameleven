﻿// https://learn.microsoft.com/en-us/dotnet/api/system.io.pipes.anonymouspipeserverstream?view=net-8.0
// https://learn.microsoft.com/en-us/dotnet/standard/io/how-to-use-anonymous-pipes-for-local-interprocess-communication

WebApplicationBuilder builder = WebApplication.CreateSlimBuilder(args);

builder.Configuration.AddCommandLine(args);

CommandLineArgumentsOptions argsOptions = builder.Configuration.Get<CommandLineArgumentsOptions>()
    ?? new CommandLineArgumentsOptions();

string pipeServerName = argsOptions.AppId is null
    ? "Emyfreya.Steam.AppEmu"
    : $"Emyfreya.Steam.AppEmu.{argsOptions.AppId}";

// Named pipes.

builder.WebHost.UseNamedPipes(options =>
{
    builder.Configuration.Bind("NamedPipeServer", options);
});

builder.WebHost.ConfigureKestrel(
    opts => opts.ListenNamedPipe(pipeServerName, p =>
    {
        p.UseHttps();

        if (builder.Environment.IsDevelopment())
        {
            p.UseConnectionLogging();
        }
    }));

builder.Services.ConfigureHttpJsonOptions(options =>
{
    options.SerializerOptions.TypeInfoResolverChain.Insert(0, AppJsonSerializerContext.Default);
});

// Endpoints.

builder.Services.AddFastEndpoints(o =>
{
    o.DisableAutoDiscovery = true;
    o.SourceGeneratorDiscoveredTypes.AddRange([
        typeof(GetAchievementsEndPoint),
        typeof(ExitEndPoint),
        typeof(IsSubcribedAppEndPoint)
    ]);
});

// Steam services.

builder.Services.AddSingleton(p => p.GetRequiredService<IOptions<SteamClientManagerOptions>>().Value);
builder.Services.AddSingleton<ISteamClientManager, SteamClientManager>();

builder.Services.AddOptions<SteamClientManagerOptions>()
    .Configure(o =>
    {
        o.AppId = argsOptions.AppId;
        o.BuildMethod = SteamClientBuildMethod.FromRegistry();
    });

WebApplication app = builder.Build();

app.UseFastEndpoints();

if (app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/error-development");
}
else
{
    app.UseExceptionHandler("/error");
}

await app.RunAsync();
