﻿namespace Emyfreya.Steam.AppEmu;

public sealed record CommandLineArgumentsOptions
{
    [ConfigurationKeyName("app-id")]
    public uint? AppId { get; init; }
}
