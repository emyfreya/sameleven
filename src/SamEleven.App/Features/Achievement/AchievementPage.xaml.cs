﻿// Copyright (c) Microsoft Corporation and Contributors.
// Licensed under the MIT License.

using Microsoft.UI.Xaml.Controls;

namespace SamEleven.App.Features.Achievement;

public sealed partial class AchievementPage : Page
{
    public AchievementPage(AchievementPageViewModel viewModel)
    {
        InitializeComponent();
        DataContext = viewModel;
    }
}
