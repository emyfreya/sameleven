﻿namespace SamEleven.App.Features.Achievement;

public sealed partial class AchievementPageViewModel : ObservableObject, IRecipient<FrameNavigated>, IDisposable
{
    [ObservableProperty]
    private SteamApp? _app;

    [ObservableProperty]
    private bool _isLoading;

    private readonly ISteamService _steamService;
    private readonly WeakReferenceMessenger _messenger;

    public AchievementPageViewModel(ISteamService steamService, WeakReferenceMessenger messenger)
    {
        _steamService = steamService;
        _messenger = messenger;

        _messenger.Register(this);
    }

    public void Dispose()
    {
        _messenger.UnregisterAll(this);
    }

    public void Receive(FrameNavigated message)
    {
        App = _messenger.Send<AppSelectedRequestMessage>();

        if (App is { } app)
        {
            Task.Run(() => InitializeAsync(app));
        }
    }

    public async Task InitializeAsync(SteamApp app)
    {
        // Based on BIN ??
        // https://github.com/gibbed/SteamAchievementManager/blob/7b301fa56acb7f1130a0a2a24368c4f3da3ebe1b/SAM.Game/Manager.cs#L229

        // Try to find something not based on the cache

        await _steamService.GetAllAchievementsAsync(app.Id, CancellationToken.None).ConfigureAwait(true);

        // Wrapper libraries do this
        // 
        // Maybe setting it and calling
        // ISteamUserStats > virtual uint32 GetNumAchievements() = 0;
        // could work
        // This would allow to know if a game has achievements
        // then calling
        // 	virtual const char *GetAchievementName( uint32 iAchievement ) = 0;
        // to have a list to show to the user
    }
}
