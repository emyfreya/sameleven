﻿namespace SamEleven.App.Features.Picker;

public sealed record AppSelectedMessage(SteamApp Game);
