﻿namespace SamEleven.App.Features.Picker;

public sealed partial class PickerPageViewModel : ObservableObject, IRecipient<FrameNavigated>, IDisposable
{
    [ObservableProperty]
    private ObservableCollection<SteamApp> _apps;

    [ObservableProperty]
    private bool _isSearchAvailable;

    private readonly List<SteamApp> _appsCache;
    private readonly WeakReferenceMessenger _messenger;
    private readonly ISteamService _steamService;
    private readonly IDispatcherQueueService _dispatcherQueue;
    private CancellationTokenSource? _searchTokenSource;

    public PickerPageViewModel(WeakReferenceMessenger messenger, ISteamService steamService, IDispatcherQueueService dispatcherQueue)
    {
        _appsCache = [];
        _messenger = messenger;
        _steamService = steamService;
        _dispatcherQueue = dispatcherQueue;

        _messenger.RegisterAll(this);
        Apps = new ObservableCollection<SteamApp>();
    }

    public void Dispose()
    {
        _searchTokenSource?.Cancel();
        _searchTokenSource?.Dispose();

        _messenger.UnregisterAll(this);
    }

    public void Receive(FrameNavigated message)
    {
        // So it doesn't get called more than once.
        _messenger.Unregister<IRecipient<FrameNavigated>>(this);

        Task.Run(LoadGamesAsync);
    }

    public void SelectGame(SteamApp game)
    {
        _messenger.Send(new AppSelectedMessage(game));
    }

    public Task SearchAsync(string? query)
    {
        _searchTokenSource?.Cancel();
        _searchTokenSource?.Dispose();

        _searchTokenSource = new CancellationTokenSource();

        Apps = new ObservableCollection<SteamApp>();
        return Task.Run(() => SearchAsync(query, _searchTokenSource.Token));
    }

    private async ValueTask LoadGamesAsync()
    {
        await foreach (SteamApp app in _steamService.GetAllAppsAsync().ConfigureAwait(false))
        {
            _appsCache.Add(app);

            await AddAppAsync(app, CancellationToken.None).ConfigureAwait(false);
        }

        await _dispatcherQueue.Enqueue(() => IsSearchAvailable = true).ConfigureAwait(false);
    }

    private Task AddAppAsync(SteamApp app, CancellationToken cancellationToken = default)
    {
        return _dispatcherQueue.Enqueue(() => Apps.Add(app), cancellationToken);
    }

    private async Task SearchAsync(string? query, CancellationToken cancellationToken = default)
    {
        foreach (SteamApp item in BuildSearchQuery(query))
        {
            if (cancellationToken.IsCancellationRequested) return;

            await AddAppAsync(item, cancellationToken).ConfigureAwait(false);
        }
    }

    private IEnumerable<SteamApp> BuildSearchQuery(string? query)
    {
        IEnumerable<SteamApp> gamesQuery = _appsCache;

        if (query is not null)
        {
            gamesQuery = gamesQuery.Where(game => game.Name.Contains(query, StringComparison.InvariantCultureIgnoreCase));
        }

        return gamesQuery.OrderBy(e => e.Name);
    }
}
