﻿
namespace SamEleven.App.Abstractions;

public interface ISteamService
{
    Task GetAllAchievementsAsync(uint appId, CancellationToken none);
    IAsyncEnumerable<SteamApp> GetAllAppsAsync(CancellationToken cancellationToken = default);
}
