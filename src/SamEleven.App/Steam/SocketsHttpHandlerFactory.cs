﻿namespace SamEleven.App.Steam;

internal sealed partial class SocketsHttpHandlerFactory : IDisposable
{
    internal const string OptionsAppIdKey = "appId";
    private const string ProcessName = "emyfreya_steam_app_emu";

    static partial class Log
    {
        [LoggerMessage(LogLevel.Information, "Created process with arguments '{Arguments}'")]
        public static partial void CreatedProcess(ILogger logger, string arguments);
    }

    private readonly ConcurrentDictionary<uint, Process> _processCache;
    private readonly AppEmuServerOptions _options;
    private readonly ILogger _logger;

    public SocketsHttpHandlerFactory(IOptions<AppEmuServerOptions> appEmuOptions, ILogger<SocketsHttpHandlerFactory> logger)
    {
        _processCache = new ConcurrentDictionary<uint, Process>(concurrencyLevel: 2, capacity: 5);
        _options = appEmuOptions.Value;
        _logger = logger;
    }

    public SocketsHttpHandler Create()
    {
        return new SocketsHttpHandler
        {
            ConnectCallback = async (context, ct) =>
            {
                HttpRequestOptionsKey<uint> key = new("appId");
                string pipeName = context.InitialRequestMessage.Options.TryGetValue(key, out uint appId)
                    ? $"Emyfreya.Steam.AppEmu.{appId}"
                    : "Emyfreya.Steam.AppEmu";

                NamedPipeClientStream pipeClientStream = new(
                    serverName: ".",
                    pipeName: pipeName,
                    PipeDirection.InOut,
                    PipeOptions.Asynchronous | PipeOptions.CurrentUserOnly);

                await pipeClientStream.ConnectAsync(ct).ConfigureAwait(false);

                return pipeClientStream;
            },
            MaxConnectionsPerServer = 1
        };
    }

    private Process GetOrCreateProcess(uint appId)
    {
        return _processCache.GetOrAdd(appId, CreateProcess);
    }

    private Process CreateProcess(uint appId)
    {
#if DEBUG
        // Only for IDE / Multiple startup projects / Debug purposes.
        if (Process.GetProcessesByName(ProcessName).FirstOrDefault() is { } inProcess) return inProcess;
#endif

        Process pipeServer = new();

        pipeServer.StartInfo.WorkingDirectory = Path.GetDirectoryName(_options.SteamAppEmuExePath);
        pipeServer.StartInfo.FileName = Path.GetFileName(_options.SteamAppEmuExePath);
        pipeServer.StartInfo.UseShellExecute = false;
        pipeServer.StartInfo.Arguments = BuildArguments(new Dictionary<string, object?>
        {
            { "--app-id", appId },
        });

        pipeServer.Exited += PipeServerExited;

        Log.CreatedProcess(_logger, pipeServer.StartInfo.Arguments);

        return pipeServer;

        void PipeServerExited(object? sender, EventArgs e)
        {
            pipeServer.Exited -= PipeServerExited;
            pipeServer.Dispose();

            _processCache.TryRemove(appId, out _);
        }
    }

    private static string BuildArguments(IEnumerable<KeyValuePair<string, object?>> keyValuePairs)
    {
        StringBuilder builder = new();

        foreach (KeyValuePair<string, object?> item in keyValuePairs)
        {
            if (item.Value is null) continue;

            builder.Append(item.Key)
                .Append(' ')
                .Append(item.Value);
        }

        return builder.ToString();
    }

    public void Dispose()
    {
        foreach (Process item in _processCache.Values.AsParallel())
        {
            item.Close();
            item.Dispose();
        }
    }
}
