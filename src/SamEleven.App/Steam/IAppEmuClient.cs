﻿namespace SamEleven.App.Steam;

internal interface IAppEmuClient
{
    [Get("/user/stats/achievements")]
    Task<GetAchievementsResponse> GetAchievementsAsync([Property(SocketsHttpHandlerFactory.OptionsAppIdKey)] uint appId, CancellationToken cancellationToken = default);

    [Get("/apps/{appId}/issubcribed")]
    Task<GetIsSubscribedResponse> IsSubcribedAppAsync(uint appId, CancellationToken cancellationToken = default);
}

internal sealed record GetIsSubscribedResponse(bool IsSubscribed);
internal sealed record GetAchievementsResponse(int Count, string[] Achievements);
