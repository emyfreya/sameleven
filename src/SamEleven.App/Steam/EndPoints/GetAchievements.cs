﻿namespace SamEleven.App.Steam.EndPoints;

internal sealed class GetAchievements
{
    public Task<string[]> HandleAsync(uint appId, CancellationToken cancellationToken)
    {
        return Task.FromResult(Array.Empty<string>());
    }
}
