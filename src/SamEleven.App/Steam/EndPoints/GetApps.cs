﻿namespace SamEleven.App.Steam;

internal sealed partial class GetApps
{
    private static partial class Log
    {
        [LoggerMessage(LogLevel.Debug, "Fetching subscribed AppIds from AppIds of length {Length}.")]
        public static partial void FetchingAppIds(ILogger logger, int length);
        [LoggerMessage(LogLevel.Information, "Found {Subscribed} subscribed AppIds in {Elapsed}ms from total of {Length} AppIds.")]
        public static partial void FoundSubscribedAppIds(ILogger logger, ushort subscribed, long elapsed, int length);
        [LoggerMessage(LogLevel.Information, "Found {Subscribed} subscribed AppIds in cache.")]
        public static partial void FoundSubscribedAppIdsCache(ILogger logger, int subscribed);
        [LoggerMessage(LogLevel.Trace, "User not subscribed to app '{AppId}'. Error={Error}")]
        public static partial void IsSubcribedAppFailed(ILogger logger, uint appId, IError? error);
    }

    private readonly ISteamApi _steamWebApi;
    private readonly ILogger _logger;
    private readonly IFileCacheService _fileCacheService;
    private readonly IAppEmuClient _appEmuClient;

    public GetApps(
        ISteamApi steamWebApi,
        ILogger<SteamService> logger,
        IFileCacheService fileCacheService,
        IAppEmuClient appEmuClient)
    {
        _steamWebApi = steamWebApi;
        _logger = logger;
        _fileCacheService = fileCacheService;
        _appEmuClient = appEmuClient;
    }

    public async IAsyncEnumerable<SteamApp> HandleAsync([EnumeratorCancellation] CancellationToken cancellationToken)
    {
        IReadOnlyList<SteamApp> cached = await GetAllAppsFromCacheAsync(cancellationToken).ConfigureAwait(false);

        if (cached.Count > 0)
        {
            foreach (SteamApp app in cached)
            {
                yield return app;
            }
        }
        else
        {
            List<SteamApp> apps = new(capacity: 100);

            await foreach (SteamApp app in GetAllAppsFromWebApiAsync(cancellationToken))
            {
                apps.Add(app);
                yield return app;
            }

            // await _fileCacheService.SaveAllAppsAsync(_steamClient, apps, cancellationToken).ConfigureAwait(false);
        }
    }

    private Task<IReadOnlyList<SteamApp>> GetAllAppsFromCacheAsync(CancellationToken cancellationToken)
    {
        IReadOnlyList<SteamApp> cached = Array.Empty<SteamApp>();

        Log.FoundSubscribedAppIdsCache(_logger, cached.Count);

        return Task.FromResult(cached);
    }

    private async IAsyncEnumerable<SteamApp> GetAllAppsFromWebApiAsync([EnumeratorCancellation] CancellationToken cancellationToken)
    {
        using ApiResponse<GetAppListResult> appList = await _steamWebApi.GetAppListAsync(cancellationToken: cancellationToken).ConfigureAwait(false);

        if (!appList.IsSuccessStatusCode) yield break;

        GetAppListAppResult[] apps = appList.Content.AppList.Apps;

        Log.FetchingAppIds(_logger, apps.Length);
        Stopwatch stopwatch = Stopwatch.StartNew();
        ushort subscribed = 0;

        foreach (GetAppListAppResult app in apps.OrderBy(e => e.Name))
        {
            GetIsSubscribedResponse response = await _appEmuClient.IsSubcribedAppAsync(app.AppId, cancellationToken).ConfigureAwait(false);

            if (!response.IsSubscribed) continue;

            yield return new SteamApp(app.AppId, app.Name, BuildLogoUri(app.AppId));

            subscribed++;
        }

        Log.FoundSubscribedAppIds(_logger, subscribed, stopwatch.ElapsedMilliseconds, apps.Length);
    }

    private Uri? BuildLogoUri(uint appId)
    {
        // TODO Most of the images are cached on the steam client side, in appcache/librarycache
        return new Uri($"https://cdn.cloudflare.steamstatic.com/steam/apps/{appId}/capsule_231x87.jpg");
    }
}
