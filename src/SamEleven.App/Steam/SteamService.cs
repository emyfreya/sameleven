﻿namespace SamEleven.App.Steam;

internal sealed partial class SteamService : ISteamService
{
    private readonly GetApps _getApps;
    private readonly GetAchievements _getAchievements;

    public SteamService(GetAchievements getAchievements, GetApps getApps)
    {
        _getAchievements = getAchievements;
        _getApps = getApps;
    }

    public Task GetAllAchievementsAsync(uint appId, CancellationToken cancellationToken = default)
    {
        return _getAchievements.HandleAsync(appId, cancellationToken);
    }

    public IAsyncEnumerable<SteamApp> GetAllAppsAsync(CancellationToken cancellationToken = default)
    {
        return _getApps.HandleAsync(cancellationToken);
    }
}
