﻿namespace SamEleven.App.Steam;

internal static class ServiceCollectionExtensions
{
    public static IServiceCollection AddSteamAppEmu(this IServiceCollection services, Action<AppEmuServerOptions> configure)
    {
        services.AddOptions<AppEmuServerOptions>()
            .Configure(configure);

        services.AddSingleton<SocketsHttpHandlerFactory>();

        services.AddRefitClient<IAppEmuClient>()
            .AddDefaultLogger()
            .ConfigurePrimaryHttpMessageHandler(provider => provider.GetRequiredService<SocketsHttpHandlerFactory>().Create());

        return services;
    }

    public static IServiceCollection AddSteamService(this IServiceCollection services)
    {
        services.AddScoped<ISteamService, SteamService>();

        services.AddScoped<GetApps>();
        services.AddScoped<GetAchievements>();

        return services;
    }
}
