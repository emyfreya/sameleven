﻿namespace SamEleven.App.Steam.Models;

internal sealed record SteamAchievement( 
    string Name,
    bool IsUnlocked,
    Uri Image);
