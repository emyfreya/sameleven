﻿namespace SamEleven.App.Steam;

internal sealed record AppEmuServerOptions
{
    public required string SteamAppEmuExePath { get; set; }
    public required JsonSerializerOptions JsonSerializerOptions { get; set; }
}
