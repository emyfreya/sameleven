﻿namespace SamEleven.App.UI.Navigation;

public sealed record FrameNavigated(Type ViewModelType);
