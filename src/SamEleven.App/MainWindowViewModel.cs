﻿namespace SamEleven.App;

public sealed partial class MainWindowViewModel : ObservableObject, IDisposable, IRecipient<AppSelectedMessage>
{
    public bool IsAppSelected => App is not null;

    [ObservableProperty]
    [NotifyPropertyChangedFor(nameof(IsAppSelected))]
    private SteamApp? _app;

    private readonly WeakReferenceMessenger _messenger;
    private readonly INavigationService _navigationService;

    public MainWindowViewModel(WeakReferenceMessenger messenger, INavigationService navigationService)
    {
        _messenger = messenger;
        _navigationService = navigationService;

        messenger.Register<MainWindowViewModel, AppSelectedRequestMessage>(this, Handler);
    }

    public void Receive(AppSelectedMessage message)
    {
        App = message.Game;

        _navigationService.NavigateAsync<AchievementPageViewModel>();
    }

    public void Dispose()
    {
        _messenger.UnregisterAll(this);
    }

    internal void Initialize()
    {
        _messenger.Register(this);
    }

    internal Task RequestNavigationAsync(object tag, bool isSettingsInvoked)
    {
        Type viewModelType = MapViewModelType(tag, isSettingsInvoked);

        return _navigationService.NavigateAsync(viewModelType);
    }

    internal Type GetViewModelType(object tag) => MapViewModelType(tag, isSettingsInvoked: false);

    private static void Handler(MainWindowViewModel subject, AppSelectedRequestMessage message)
    {
        message.Reply(subject.App);
    }

    private static Type MapViewModelType(object tag, bool isSettingsInvoked)
    {
        if (isSettingsInvoked) return typeof(PickerPageViewModel);

        return tag switch
        {
            "achievements" => typeof(AchievementPageViewModel),
            _ => typeof(PickerPageViewModel),
        };
    }
}
