﻿namespace SamEleven.App.Caching;

internal sealed class FileCacheServiceOptions
{
    public required string RootPath { get; set; }
}
