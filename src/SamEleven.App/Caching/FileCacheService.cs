﻿namespace SamEleven.App.Caching;

internal sealed partial class FileCacheService : IFileCacheService
{
    private static partial class Log
    {
        [LoggerMessage(LogLevel.Information, "Getting type '{Type}' from path '{Path}'")]
        public static partial void Getting(ILogger logger, Type type, string path);
        [LoggerMessage(LogLevel.Information, "Saving type '{Type}' to path '{Path}'")]
        public static partial void Saving(ILogger logger, Type type, string path);
        [LoggerMessage(LogLevel.Error, "Could not get file for '{Type}' from path '{Path}'")]
        public static partial void GetFileFailed(ILogger logger, Exception e, Type type, string path);
        [LoggerMessage(LogLevel.Error, "Could not save file for '{Type}' to path '{Path}'")]
        public static partial void SaveFileFailed(ILogger logger, Exception e, Type type, string path);
    }

    private readonly FileCacheServiceOptions _options;
    private readonly JsonSerializerOptions _serializerOptions;
    private readonly ILogger _logger;

    public FileCacheService(IOptions<FileCacheServiceOptions> options, ILogger<FileCacheService> logger)
    {
        _options = options.Value;
        _serializerOptions = new JsonSerializerOptions
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            WriteIndented = false,
            TypeInfoResolver = FileCacheJsonContext.Default
        };
        _logger = logger;
    }

    public async ValueTask<T?> GetAsync<T>(string path, CancellationToken cancellationToken = default)
    {
        string fullPath = GetPath(path);

        Log.Getting(_logger, typeof(T), fullPath);

        if (!File.Exists(fullPath)) return default;

        try
        {
            using FileStream reader = File.OpenRead(fullPath);
            return await JsonSerializer.DeserializeAsync<T>(reader, _serializerOptions, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception e)
        {
            Log.GetFileFailed(_logger, e, typeof(T), fullPath);

            return default;
        }
    }

    public async Task SaveAsync<T>(string path, T value, CancellationToken cancellationToken = default)
    {
        string fullPath = GetPath(path);
        string directoryPath = Path.GetDirectoryName(fullPath) ?? throw new InvalidOperationException($"Invalid root path: '{fullPath}'.");

        Log.Saving(_logger, typeof(T), fullPath);

        if (!Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath);
        }

        try
        {
            using FileStream reader = File.OpenWrite(fullPath);
            await JsonSerializer.SerializeAsync(reader, value, _serializerOptions, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception e)
        {
            Log.SaveFileFailed(_logger, e, typeof(T), fullPath);
        }
    }

    private string GetPath(string path) => Path.Combine(_options.RootPath, path);
}
