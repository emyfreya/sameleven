﻿namespace SamEleven.App.Tests.Caching;

public class FileCacheServiceTests
{
    private readonly ILogger<FileCacheService> _logger;

    public FileCacheServiceTests(ITestOutputHelper testOutputHelper)
    {
        _logger = new XunitLogger<FileCacheService>(testOutputHelper);
    }

    private FileCacheService Create(Action<FileCacheServiceOptions> configure)
    {
        FileCacheServiceOptions options = new()
        {
            RootPath = ""
        };
        configure(options);

        return new FileCacheService(Options.Create(options), _logger);
    }

    [Fact]
    public async Task ShouldCreateDirectoryRecursivly()
    {
        // Arrange
        Directory.Delete(Path.Combine(Directory.GetCurrentDirectory(), "cache"), true);

        FileCacheService service = Create(o =>
        {
            o.RootPath = Directory.GetCurrentDirectory();
        });

        List<SteamApp> apps = new(1)
        {
            new SteamApp(1, "", null)
        };

        // Act
        await service.SaveAsync("cache/123456/apps.json", apps);

        // Assert
        File.Exists("cache/123456/apps.json").Should().BeTrue();
    }
}
